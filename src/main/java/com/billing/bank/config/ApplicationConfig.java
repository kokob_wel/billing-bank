package com.billing.bank.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Your Spring dependency configurations go here.
 */
@Configuration
@EnableAsync
public class ApplicationConfig {

    @Bean(name = "taskExecutor")
    public Executor taskExecutor(){
        return Executors.newCachedThreadPool();
    }

}
