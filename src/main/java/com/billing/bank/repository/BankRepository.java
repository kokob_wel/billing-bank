package com.billing.bank.repository;

import com.billing.bank.model.CustomerAccount;
import com.billing.bank.controller.wire.CreditAmount;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Single point of access to the customer account data store.
 */
@Repository
@Scope("singleton")
public class BankRepository {

    private Map<String, CustomerAccount> customers;

    public BankRepository() {
        this.customers = new ConcurrentHashMap<>();
    }

    /**
     * Method for accessing customer by Id.
     * @param Id customer id.
     * @return customer account object.
     */
    public CustomerAccount getCustomerById(String Id){
        return customers.get(Id);
    }

    /**
     * A method to update the data store with a new customer account.
     * @param customerId customer id for a new account.
     * @param creditAmount initial credit amount.
     * @return customer object that have just been added.
     */
    public CustomerAccount createCustomer(String customerId, CreditAmount creditAmount){
        CustomerAccount customerAccount = new CustomerAccount(customerId);
        customerAccount.creditAccount(creditAmount);
        customers.put(customerId, customerAccount);
        return customerAccount;
    }

    /**
     * Method that deletes all available customer accounts.
     */
    public void clearRepository(){
        this.customers.clear();
    }
}