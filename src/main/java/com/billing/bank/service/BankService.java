package com.billing.bank.service;

import com.billing.bank.model.CustomerAccount;
import com.billing.bank.repository.BankRepository;
import com.billing.bank.controller.wire.CreditAmount;
import com.billing.bank.controller.wire.CustomerBalance;
import com.billing.bank.controller.wire.DebitAmount;
import com.billing.bank.controller.wire.DebitHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.util.concurrent.CompletableFuture;

@Service
public class BankService {

    @Autowired
    private BankRepository bankRepository;

    @Async
    public CompletableFuture<CustomerBalance> getCustomerBalance(String customerId) {
        CustomerAccount customerAccount = bankRepository.getCustomerById(customerId);
        if(customerAccount!=null) {
            CustomerBalance customerBalance = customerAccount.getCustomerBalance();
            return CompletableFuture.completedFuture(customerBalance);
        }else
            return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<CustomerBalance> postCredit(String customerId, CreditAmount creditAmount) {
        CustomerAccount customerAccount = bankRepository.getCustomerById(customerId);
        if(customerAccount!=null){
            CustomerBalance customerBalance = customerAccount.creditAccount(creditAmount);
            return CompletableFuture.completedFuture(customerBalance);
        }else {
            CustomerAccount customer = bankRepository.createCustomer(customerId, creditAmount);
            CustomerBalance customerBalance = customer.getCustomerBalance();
            return CompletableFuture.completedFuture(customerBalance);
        }
    }

    @Async
    public CompletableFuture<CustomerBalance> postDebit(String customerId, DebitAmount debitAmount) {
        CustomerAccount customerAccount = bankRepository.getCustomerById(customerId);
        if(customerAccount!=null) {
            CustomerBalance customerBalance = customerAccount.debitAccount(debitAmount);
            return CompletableFuture.completedFuture(customerBalance);
        }else
            return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<DebitHistory> getDebitHistory(String customerId) {
        CustomerAccount customerAccount = bankRepository.getCustomerById(customerId);
        if(customerAccount!=null) {
            DebitHistory debitHistory = customerAccount.getDebitHistory();
            return CompletableFuture.completedFuture(debitHistory);
        }else
            return CompletableFuture.completedFuture(null);
    }
}
