package com.billing.bank.model;

import com.billing.bank.controller.wire.*;
import javafx.util.Pair;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class CustomerAccount {

    private String customerId;
    private Long totalAvailableBalance;
    private Queue<CreditAmount> creditEntries;
    private DebitHistory debitHistory;
    public static final String DEBIT_ERROR_MESSAGE = "Not enough balance to withdraw";

    public CustomerAccount(final String customerId) {
        this.customerId = customerId;
        this.totalAvailableBalance = 0L;
        this.creditEntries = new ConcurrentLinkedQueue<>();
        this.debitHistory = new DebitHistory();
        initializeDebitHistory(debitHistory);
    }

    /**
     * customer id accessor method.
     * @return customer Id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Method that constructs customer balance object from the credit Entries queue.
     * @return customer balance object.
     */
    public CustomerBalance getCustomerBalance() {
        return constructCustomerBalance(creditEntries);
    }

    /**
     * History object accessor.
     * @return history object.
     */
    public DebitHistory getDebitHistory() {
        return debitHistory;
    }

    /**
     * A synchronized method that performs credit
     * @param creditAmount credit amount object.
     * @return customer balance object.
     */
    public synchronized CustomerBalance creditAccount(final CreditAmount creditAmount){
        assert(creditAmount != null && creditAmount.getMoney().getAmount() > 0);
        String transactionId = creditAmount.getTransactionId();
        CreditType creditType = creditAmount.getCreditType();
        if(!creditEntryAlreadyExists(transactionId, creditType)){
            creditEntries.add(creditAmount);
            totalAvailableBalance += creditAmount.getMoney().getAmount();
        }
        return constructCustomerBalance(creditEntries);
    }

    /**
     * A private utility method that checks if an entry already exists for Idempotence.
     * @param transactionId transaction id associated with a given credit.
     * @param creditType credit type.
     * @return boolean flag. True if exists, false otherwise.
     */
    private boolean creditEntryAlreadyExists(final String transactionId, final CreditType creditType) {
        return creditEntries.stream()
                .anyMatch(creditEntry -> creditEntry.getTransactionId().equals(transactionId)
                        && creditEntry.getCreditType().equals(creditType));
    }

    /**
     * A synchronized method that performs debit.
     * @param debitAmount debit amount object.
     * @return customer balance object.
     */
    public synchronized CustomerBalance debitAccount(final DebitAmount debitAmount){
        assert(debitAmount != null && debitAmount.getMoney().getAmount() > 0);
        String invoiceId = debitAmount.getInvoiceId();
        if(!debitEntryAlreadyExists(invoiceId)) {
            Long amount_requested = debitAmount.getMoney().getAmount();
            if(totalAvailableBalance >= amount_requested){
                totalAvailableBalance -= amount_requested;
                Pair<String, CreditType> txnIdCreditTypePair = updateCreditEntriesAfterDebit(amount_requested);
                updateDebitHistory(debitAmount,txnIdCreditTypePair);
            }else{
                throw new RuntimeException(DEBIT_ERROR_MESSAGE);
            }
        }
        return constructCustomerBalance(creditEntries);
    }

    /**
     * A private utility method that updates existing debit history.
     * @param debitAmount
     * @param txnIdCreditTypePair
     */
    private void updateDebitHistory(final DebitAmount debitAmount,final Pair<String,CreditType> txnIdCreditTypePair) {
        DebitLineItem debitLineItem = new DebitLineItem();
        debitLineItem.setTransactionId(txnIdCreditTypePair.getKey());
        debitLineItem.setCreditType(txnIdCreditTypePair.getValue());
        debitLineItem.setAmount(debitAmount.getMoney());
        debitLineItem.setInvoiceId(debitAmount.getInvoiceId());
        debitLineItem.setTransactionDate(Instant.now());
        debitHistory.getDebits().add(debitLineItem);
    }

    /**
     * A private utility method that updates credit entry queue for Idempotence and priority management.
     * @param debitAmount debit amount object.
     * @return pair object of transaction id and credit type of the last debited amount.
     */
    private Pair<String, CreditType> updateCreditEntriesAfterDebit(final Long debitAmount) {
        Long requestedDebitAmount = debitAmount;
        CreditType debitedCreditType = null;
        String transactionId = "";
        Iterator<CreditAmount> iterator = creditEntries.iterator();
        while (iterator.hasNext() && requestedDebitAmount > 0) {
            CreditAmount nextCreditAmount = iterator.next();
            debitedCreditType = nextCreditAmount.getCreditType();
            transactionId = nextCreditAmount.getTransactionId();
            Long creditAmount = nextCreditAmount.getMoney().getAmount();
            if(requestedDebitAmount>=creditAmount){
                requestedDebitAmount -=creditAmount;
                creditEntries.remove();
            }else{
                Long amount = nextCreditAmount.getMoney().getAmount();
                nextCreditAmount.getMoney().setAmount(amount-requestedDebitAmount);
                requestedDebitAmount = 0L;
            }
        }
        return new Pair<>(transactionId,debitedCreditType);
    }

    /**
     * A private utility method that checks if an entry already exists for Idempotence.
     * @param invoiceId invoice id.
     * @return boolean flag. True if exists, false otherwise.
     */
    private boolean debitEntryAlreadyExists(final String invoiceId) {
        return debitHistory.getDebits().stream()
                .anyMatch(debitEntry -> debitEntry.getInvoiceId().equals(invoiceId));
    }

    /**
     * A private utility method that constructs customer balance object from the existing credit entries.
     * @param creditEntries credit entry.
     * @return customer balance object.
     */
    private CustomerBalance constructCustomerBalance(Queue<CreditAmount> creditEntries) {
        CustomerBalance customerBalance = new CustomerBalance();
        initializeCustomerBalance(customerBalance);
        Map<CreditType, List<Money>> balanceAmounts = customerBalance.getBalanceAmounts();
        creditEntries.forEach(creditAmount -> {
            CreditType creditType = creditAmount.getCreditType();
            List<Money> monies = balanceAmounts.get(creditType);
            if(monies == null){
                monies = new ArrayList<>();
            }
            monies.add(creditAmount.getMoney());
            balanceAmounts.put(creditType,monies);
        });
        return customerBalance;
    }

    /**
     * method that instantiates customer balance object.
     * @param customerBalance customer balance object.
     */
    private void initializeCustomerBalance(CustomerBalance customerBalance) {
        Map<CreditType, List<Money>> balanceAmounts = new LinkedHashMap<>(100, 0.75f, false);
        customerBalance.setBalanceAmounts(balanceAmounts);
    }

    /**
     * method that instantiates debit history object.
     * @param debitHistory debit history object.
     */
    private void initializeDebitHistory(DebitHistory debitHistory) {
        List<DebitLineItem> debits = new LinkedList<>();
        debitHistory.setDebits(debits);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerAccount)) return false;
        CustomerAccount that = (CustomerAccount) o;
        return getCustomerId().equals(that.getCustomerId());
    }

    @Override
    public int hashCode() {
        return getCustomerId().hashCode();
    }

    @Override
    public String toString() {
        return "CustomerAccount{" +
                "customerId='" + customerId + '\'' +
                ", totalAvailableBalance=" + totalAvailableBalance +
                ", creditEntries=" + creditEntries +
                ", debitHistory=" + debitHistory +
                '}';
    }
}
