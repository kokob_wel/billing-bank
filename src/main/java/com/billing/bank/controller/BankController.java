package com.billing.bank.controller;

import com.billing.bank.controller.wire.CreditAmount;
import com.billing.bank.controller.wire.DebitAmount;
import com.billing.bank.controller.wire.DebitHistory;
import com.billing.bank.controller.wire.CustomerBalance;
import com.billing.bank.service.BankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.concurrent.ExecutionException;


/**
 * Implement the following methods to complete the exercise.
 */
@RestController
public class BankController {

//    E.g. way to wire in dependencies. Note that there are other ways like constructor injection, setter injection etc.
//    @Autowired
//    private SomeDependency someDependency;

    @Autowired
    private BankService bankService;

    Logger logger = LoggerFactory.getLogger(BankController.class);

    /**
     *
     * @param customerId String id representing the customer/account id.
     * @return How much money is left in the customer's account, i.e, After adding all the credits,
     * and subtracting all the debits, how much money is left.
     */
    @GetMapping("customer/{customerId}/balance")
    public CustomerBalance getBalance(@PathVariable String customerId) throws ExecutionException, InterruptedException {
        logger.info(String.format("Balance for customer-Id %s requested",customerId));
        CustomerBalance customerBalance = bankService.getCustomerBalance(customerId).get();
        return customerBalance;
    }

    /**
     *
     * @param customerId String id representing the customer/account id.
     * @param creditAmount How much credit should be applied to the account
     * @return How much money is left in the customer's account after the credit was applied.
     */
    @PostMapping(value = "customer/{customerId}/credit")
    public CustomerBalance postCredit(@PathVariable String customerId, @RequestBody CreditAmount creditAmount) throws ExecutionException, InterruptedException {
        logger.info("Credit of "+creditAmount.toString() +" requested for deposit for Customer-id " + customerId);
        CustomerBalance customerBalance = bankService.postCredit(customerId, creditAmount).get();
        return customerBalance;
    }

    /**
     *
     * @param customerId String id representing the customer/account id.
     * @param debitAmount How much money should be deducted from the customer's balance
     * @return How much money is left in the customer's account after the debit amount was deducted from balance.
     */
    @PostMapping(value = "customer/{customerId}/debit" , consumes = "application/json")
    public CustomerBalance debit(@PathVariable String customerId, @RequestBody DebitAmount debitAmount) throws ExecutionException, InterruptedException {
        logger.info("Debit of "+debitAmount +" requested for withdrawal for Customer-id " + customerId);
        CustomerBalance customerBalance = bankService.postDebit(customerId, debitAmount).get();
        return customerBalance;
    }

    /**
     *
     * @param customerId String id representing the customer/account id.
     * @return The debitHistory object representing all the debit transactions made to the customer's account.
     */
    @GetMapping("customer/{customerId}/history")
    public DebitHistory debitHistory(@PathVariable String customerId) throws ExecutionException, InterruptedException {
        logger.info("Debit history requested for Customer-id " + customerId);
        DebitHistory debitHistory = bankService.getDebitHistory(customerId).get();
        return debitHistory;
    }

}
