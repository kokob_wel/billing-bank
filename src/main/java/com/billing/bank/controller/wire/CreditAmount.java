package com.billing.bank.controller.wire;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * Wire object representing a credit that the customer is trying to push to their account. The customer's balance will
 * increase once the credit represented here is applied to their account.
 */
public class CreditAmount {

    //Id related to the cash transaction that facilitated this balance transfer. This should be unique for a given creditType.
    @JsonProperty("transactionId")
    private String transactionId;

    //Money object representing the amount left in the account.
    @JsonProperty("money")
    private Money money;

    //Type of credit. Different types of credits cannot be merged with each other.
    @JsonProperty("creditType")
    private CreditType creditType;

    public CreditAmount() {
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public CreditType getCreditType() {
        return creditType;
    }

    public void setCreditType(CreditType creditType) {
        this.creditType = creditType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditAmount)) return false;
        CreditAmount that = (CreditAmount) o;
        return Objects.equals(getTransactionId(), that.getTransactionId()) &&
                Objects.equals(getMoney(), that.getMoney()) &&
                getCreditType() == that.getCreditType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTransactionId(), getMoney(), getCreditType());
    }

    @Override
    public String toString() {
        return "CreditAmount{" +
                "transactionId='" + transactionId + '\'' +
                ", money=" + money +
                ", creditType=" + creditType +
                '}';
    }
}
