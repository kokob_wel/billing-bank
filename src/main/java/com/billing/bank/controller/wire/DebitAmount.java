package com.billing.bank.controller.wire;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * Wire objects representing debit transactions to the customer account. The amount represented here is by how much
 * a customer's balance will go down, once the debit transaction is applied.
 */
public class DebitAmount {
    @JsonProperty
    private String invoiceId; //Id denoting the receipt for the charge. Should be unique for a given customer.
    @JsonProperty
    private Money money;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DebitAmount)) return false;
        DebitAmount that = (DebitAmount) o;
        return Objects.equals(getInvoiceId(), that.getInvoiceId()) &&
                Objects.equals(getMoney(), that.getMoney());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInvoiceId(), getMoney());
    }

    @Override
    public String toString() {
        return "DebitAmount{" +
                "invoiceId='" + invoiceId + '\'' +
                ", money=" + money +
                '}';
    }
}
