package com.billing.bank;

import com.billing.bank.controller.wire.*;
import com.billing.bank.model.CustomerAccount;
import com.billing.bank.repository.BankRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.billing.bank.service.BankService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BillingbankApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private BankService bankService;

	@Autowired
	private BankRepository bankRepository;

	@Before
	public void setUp(){
		bankRepository.clearRepository();
	}

	@Test
	public void testPostCredit() {
		String customerId = "1";
		Long moneyAmount = 100L;
		String creditedCurrency = "USD";
		CreditType creditedType = CreditType.CASH;
		String transactionId = "crdTxn1Cst1";
		String postUrl = "/customer/" + customerId + "/credit";

		CreditAmount creditAmount = buildCreditAmount(moneyAmount, creditedCurrency, creditedType, transactionId);
		CustomerBalance expectedCustomerBalance = buildCustomerBalance(creditAmount);
		//test creating and crediting account with CASH
		makeAndAssertPostRequest(postUrl, creditAmount, expectedCustomerBalance);
		//test idempotence
		makeAndAssertPostRequest(postUrl, creditAmount, expectedCustomerBalance);

		Long moneyAmount2 = 50L;
		CreditType creditedType2 = CreditType.GIFTCARD;
		CreditAmount creditAmount2 = buildCreditAmount(moneyAmount2, creditedCurrency, creditedType2, transactionId);
		CustomerBalance expectedCustomerBalance2 = buildCustomerBalance(creditAmount,creditAmount2);
		//test crediting account account with GIFTCARD
		makeAndAssertPostRequest(postUrl, creditAmount2, expectedCustomerBalance2);
		//test idempotence
		makeAndAssertPostRequest(postUrl, creditAmount2, expectedCustomerBalance2);

		Long moneyAmount3 = 25L;
		CreditType creditedType3 = CreditType.PROMOTION;
		CreditAmount creditAmount3 = buildCreditAmount(moneyAmount3, creditedCurrency, creditedType3, transactionId);
		CustomerBalance expectedCustomerBalance3 = buildCustomerBalance(creditAmount,creditAmount2,creditAmount3);
		//test crediting account account with PROMOTION
		makeAndAssertPostRequest(postUrl, creditAmount3, expectedCustomerBalance3);
		//test idempotence
		makeAndAssertPostRequest(postUrl, creditAmount3, expectedCustomerBalance3);

		Long moneyAmount4 = 30L;
		CreditType creditedType4 = CreditType.GIFTCARD;
		String transactionId2 = "crdTxn2Cst1";
		CreditAmount creditAmount4 = buildCreditAmount(moneyAmount4, creditedCurrency, creditedType4, transactionId2);
		CustomerBalance expectedCustomerBalance4 = buildCustomerBalance(creditAmount,creditAmount2,creditAmount3,creditAmount4);
		//test crediting additional account account with GIFTCARD
		makeAndAssertPostRequest(postUrl, creditAmount4, expectedCustomerBalance4);
		//test idempotence
		makeAndAssertPostRequest(postUrl, creditAmount4, expectedCustomerBalance4);

		Long moneyAmount5 = 30L;
		CreditType creditedType5 = CreditType.GIFTCARD;
		CreditAmount creditAmount5 = buildCreditAmount(moneyAmount5, creditedCurrency, creditedType5, transactionId);
		//test crediting additional account account with duplicated GIFTCARD and transactionId. Expect previous balance.
		makeAndAssertPostRequest(postUrl, creditAmount5, expectedCustomerBalance4);
		//test idempotence
		makeAndAssertPostRequest(postUrl, creditAmount5, expectedCustomerBalance4);
	}

	@Test
	public void testPostDebit(){

		String customerId = "1";
		String currency = "USD";
		String debitPostUrl = "/customer/" + customerId + "/debit";

		String transactionId = "crdTxn1Cst1";
		String creditPostUrl = "/customer/" + customerId + "/credit";
		Long moneyAmount1 = 100L;
		CreditType creditedType = CreditType.CASH;
		CreditAmount creditAmount1 = buildCreditAmount(moneyAmount1, currency, creditedType, transactionId);
		CustomerBalance expectedCustomerBalance = buildCustomerBalance(creditAmount1);
		//Crediting account with CASH
		makeAndAssertPostRequest(creditPostUrl, creditAmount1, expectedCustomerBalance);

		Long moneyAmount2 = 50L;
		CreditType creditedType2 = CreditType.GIFTCARD;
		CreditAmount creditAmount2 = buildCreditAmount(moneyAmount2, currency, creditedType2, transactionId);
		CustomerBalance expectedCustomerBalance2 = buildCustomerBalance(creditAmount1,creditAmount2);
		//Crediting account account with GIFTCARD
		makeAndAssertPostRequest(creditPostUrl, creditAmount2, expectedCustomerBalance2);

		Long moneyAmount3 = 25L;
		CreditType creditedType3 = CreditType.PROMOTION;
		CreditAmount creditAmount3 = buildCreditAmount(moneyAmount3, currency, creditedType3, transactionId);
		CustomerBalance expectedCustomerBalance3 = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3);
		//Crediting account account with PROMOTION
		makeAndAssertPostRequest(creditPostUrl, creditAmount3, expectedCustomerBalance3);

		Long moneyAmount4 = 30L;
		CreditType creditedType4 = CreditType.GIFTCARD;
		String transactionId2 = "crdTxn2Cst1";
		CreditAmount creditAmount4 = buildCreditAmount(moneyAmount4, currency, creditedType4, transactionId2);
		CustomerBalance expectedCustomerBalance4 = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3,creditAmount4);
		//Crediting additional account account with GIFTCARD
		makeAndAssertPostRequest(creditPostUrl, creditAmount4, expectedCustomerBalance4);

		Long debitMoneyAmount1 = 80L;
		String invoiceId1 = "dbtTxn1Cst1";
		DebitAmount debitAmount = buildDebitAmount(debitMoneyAmount1, currency, invoiceId1);

		Long expectedAmountAfterDebit = creditAmount1.getMoney().getAmount() - debitMoneyAmount1;
		creditAmount1.getMoney().setAmount(expectedAmountAfterDebit);
		CustomerBalance expectedCustomerBalanceAfterDebit = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3,creditAmount4);
		//test debiting amount
		makeAndAssertPostRequest(debitPostUrl, debitAmount, expectedCustomerBalanceAfterDebit);
		// test idempotence
		makeAndAssertPostRequest(debitPostUrl, debitAmount, expectedCustomerBalanceAfterDebit);

		Long debitMoneyAmount2 = 25L;
		String invoiceId2 = "dbtTxn2Cst1";
		DebitAmount debitAmount2 = buildDebitAmount(debitMoneyAmount2, currency, invoiceId2);

		//expect debit to be applied partially from credit 1 and credit 2
		Long expectedAmountAfterDebit2 = (creditAmount1.getMoney().getAmount() + creditAmount2.getMoney().getAmount())- debitMoneyAmount2;
		creditAmount2.getMoney().setAmount(expectedAmountAfterDebit2);
		CustomerBalance expectedCustomerBalanceAfterDebit2 = buildCustomerBalance(creditAmount2,creditAmount3,creditAmount4);
		//test debiting amount
		makeAndAssertPostRequest(debitPostUrl, debitAmount2, expectedCustomerBalanceAfterDebit2);
		//test idempotence
		makeAndAssertPostRequest(debitPostUrl, debitAmount2, expectedCustomerBalanceAfterDebit2);
	}

	@Test(expected = RuntimeException.class)
	public void testPostDebitWithOverDraft(){

		String customerId = "1";
		String currency = "USD";
		String debitPostUrl = "/customer/" + customerId + "/debit";

		String transactionId = "crdTxn1Cst1";
		String creditPostUrl = "/customer/" + customerId + "/credit";
		Long moneyAmount1 = 100L;
		CreditType creditedType = CreditType.CASH;
		CreditAmount creditAmount1 = buildCreditAmount(moneyAmount1, currency, creditedType, transactionId);
		CustomerBalance expectedCustomerBalance = buildCustomerBalance(creditAmount1);
		//Crediting account with CASH
		makeAndAssertPostRequest(creditPostUrl, creditAmount1, expectedCustomerBalance);

		Long moneyAmount2 = 50L;
		CreditType creditedType2 = CreditType.GIFTCARD;
		CreditAmount creditAmount2 = buildCreditAmount(moneyAmount2, currency, creditedType2, transactionId);
		CustomerBalance expectedCustomerBalance2 = buildCustomerBalance(creditAmount1,creditAmount2);
		//Crediting account account with GIFTCARD
		makeAndAssertPostRequest(creditPostUrl, creditAmount2, expectedCustomerBalance2);

		Long moneyAmount3 = 25L;
		CreditType creditedType3 = CreditType.PROMOTION;
		CreditAmount creditAmount3 = buildCreditAmount(moneyAmount3, currency, creditedType3, transactionId);
		CustomerBalance expectedCustomerBalance3 = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3);
		//Crediting account account with PROMOTION
		makeAndAssertPostRequest(creditPostUrl, creditAmount3, expectedCustomerBalance3);

		Long debitMoneyAmount3 = 200L;
		String invoiceId3 = "dbtTxn3Cst1";
		DebitAmount debitAmount3 = buildDebitAmount(debitMoneyAmount3, currency, invoiceId3);
		CustomerBalance dummyExpectedBalance = new CustomerBalance();

		//expect debit to fail due to low balance.
		try {
			//test debiting amount more than total available balance.
			makeAndAssertPostRequest(debitPostUrl, debitAmount3, dummyExpectedBalance);
		}catch (Exception e) {
			assertTrue(e.getMessage().contains(CustomerAccount.DEBIT_ERROR_MESSAGE));
			throw e;
		}
	}

	@Test
	public void testGetBalance(){

		String customerId = "1";
		String currency = "USD";
		String balanceGetUrl = "/customer/" + customerId + "/balance";

		String transactionId = "crdTxn1Cst1";
		String creditPostUrl = "/customer/" + customerId + "/credit";
		Long moneyAmount1 = 100L;
		CreditType creditedType = CreditType.CASH;
		CreditAmount creditAmount1 = buildCreditAmount(moneyAmount1, currency, creditedType, transactionId);
		CustomerBalance expectedCustomerBalance = buildCustomerBalance(creditAmount1);
		//Crediting account with CASH
		makeAndAssertPostRequest(creditPostUrl, creditAmount1, expectedCustomerBalance);

		Long moneyAmount2 = 50L;
		CreditType creditedType2 = CreditType.GIFTCARD;
		CreditAmount creditAmount2 = buildCreditAmount(moneyAmount2, currency, creditedType2, transactionId);
		CustomerBalance expectedCustomerBalance2 = buildCustomerBalance(creditAmount1,creditAmount2);
		//Crediting account account with GIFTCARD
		makeAndAssertPostRequest(creditPostUrl, creditAmount2, expectedCustomerBalance2);

		Long moneyAmount3 = 25L;
		CreditType creditedType3 = CreditType.PROMOTION;
		CreditAmount creditAmount3 = buildCreditAmount(moneyAmount3, currency, creditedType3, transactionId);
		CustomerBalance expectedCustomerBalance3 = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3);
		//Crediting account account with PROMOTION
		makeAndAssertPostRequest(creditPostUrl, creditAmount3, expectedCustomerBalance3);

		makeAndAssertGetRequest(balanceGetUrl,expectedCustomerBalance3);
	}

	@Test
	public void testGetHistory(){

		String customerId = "2";
		String currency = "USD";
		String historyGetUrl = "/customer/" + customerId + "/history";

		String debitPostUrl = "/customer/" + customerId + "/debit";

		String transactionId = "crdTxn1Cst2";
		String creditPostUrl = "/customer/" + customerId + "/credit";
		Long moneyAmount1 = 150L;
		CreditType creditedType1 = CreditType.CASH;
		CreditAmount creditAmount1 = buildCreditAmount(moneyAmount1, currency, creditedType1, transactionId);
		CustomerBalance expectedCustomerBalance = buildCustomerBalance(creditAmount1);
		//Crediting account with CASH
		makeAndAssertPostRequest(creditPostUrl, creditAmount1, expectedCustomerBalance);

		Long moneyAmount2 = 75L;
		CreditType creditedType2 = CreditType.GIFTCARD;
		CreditAmount creditAmount2 = buildCreditAmount(moneyAmount2, currency, creditedType2, transactionId);
		CustomerBalance expectedCustomerBalance2 = buildCustomerBalance(creditAmount1,creditAmount2);
		//Crediting account account with GIFTCARD
		makeAndAssertPostRequest(creditPostUrl, creditAmount2, expectedCustomerBalance2);

		Long moneyAmount3 = 25L;
		CreditType creditedType3 = CreditType.PROMOTION;
		CreditAmount creditAmount3 = buildCreditAmount(moneyAmount3, currency, creditedType3, transactionId);
		CustomerBalance expectedCustomerBalance3 = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3);
		//Crediting account account with PROMOTION
		makeAndAssertPostRequest(creditPostUrl, creditAmount3, expectedCustomerBalance3);

		Long debitMoneyAmount1 = 30L;
		String invoiceId1 = "dbtTxn1Cst2";
		DebitAmount debitAmount = buildDebitAmount(debitMoneyAmount1, currency, invoiceId1);

		Long expectedAmountAfterDebit = creditAmount1.getMoney().getAmount() - debitMoneyAmount1;
		creditAmount1.getMoney().setAmount(expectedAmountAfterDebit);
		CustomerBalance expectedCustomerBalanceAfterDebit = buildCustomerBalance(creditAmount1,creditAmount2,creditAmount3);
		//test debiting amount
		makeAndAssertPostRequest(debitPostUrl, debitAmount, expectedCustomerBalanceAfterDebit);

		Long debitMoneyAmount2 = 120L;
		String invoiceId2 = "dbtTxn2Cst2";
		DebitAmount debitAmount2 = buildDebitAmount(debitMoneyAmount2, currency, invoiceId2);

		//testing the consumption of the first credit's entire amount
		CustomerBalance expectedCustomerBalanceAfterDebit2 = buildCustomerBalance(creditAmount2,creditAmount3);
		//test debiting amount
		makeAndAssertPostRequest(debitPostUrl, debitAmount2, expectedCustomerBalanceAfterDebit2);

		Long debitMoneyAmount3 = 50L;
		String invoiceId3 = "dbtTxn3Cst2";
		DebitAmount debitAmount3 = buildDebitAmount(debitMoneyAmount3, currency, invoiceId3);

		Long expectedAmountAfterDebit3 = creditAmount2.getMoney().getAmount() - debitMoneyAmount3;
		creditAmount2.getMoney().setAmount(expectedAmountAfterDebit3);
		CustomerBalance expectedCustomerBalanceAfterDebit3 = buildCustomerBalance(creditAmount2,creditAmount3);
		//test debiting amount
		makeAndAssertPostRequest(debitPostUrl, debitAmount3, expectedCustomerBalanceAfterDebit3);

		try {
			ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get(historyGetUrl));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[0].invoiceId").value(invoiceId1));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[0].amount.amount").value(debitMoneyAmount1));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[0].amount.currency").value(currency));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[0].transactionId").value(transactionId));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[0].creditType").value(creditedType1.name()));

			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[1].invoiceId").value(invoiceId2));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[1].amount.amount").value(debitMoneyAmount2));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[1].amount.currency").value(currency));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[1].transactionId").value(transactionId));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[1].creditType").value(creditedType1.name()));

			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[2].invoiceId").value(invoiceId3));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[2].amount.amount").value(debitMoneyAmount3));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[2].amount.currency").value(currency));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[2].transactionId").value(transactionId));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.debits[2].creditType").value(creditedType2.name()));
		} catch (Exception e) {
			throw new RuntimeException("mvc request failed with - " + e);
		}
	}

	private void makeAndAssertPostRequest(String url, Object postBody, Object responseBody) {
		try {
			mvc.perform(MockMvcRequestBuilders
					.post(url)
					.content(asJsonString(postBody))
					.contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk())
					.andExpect(content().string(asJsonString(responseBody)));
		} catch (Exception e) {
			throw new RuntimeException("mvc request failed with - " + e);
		}
	}

	private void makeAndAssertGetRequest(String url, Object responseBody) {
		try {
			mvc.perform(MockMvcRequestBuilders.get(url))
					.andExpect(status().isOk())
					.andExpect(content().string(asJsonString(responseBody)));
		} catch (Exception e) {
			throw new RuntimeException("mvc request failed with - " + e);
		}
	}

	private CustomerBalance buildCustomerBalance(CreditAmount... creditAmounts) {
		CustomerBalance customerBalance = new CustomerBalance();
		Map<CreditType, List<Money>> balanceAmounts = new LinkedHashMap<>();
		for (CreditAmount creditAmount : creditAmounts) {
			CreditType creditType = creditAmount.getCreditType();
			List<Money> monies = balanceAmounts.get(creditType);
			if(monies==null){
				monies = new ArrayList<>();
			}
			Money creditedMoney = new Money();
			creditedMoney.setAmount(creditAmount.getMoney().getAmount());
			creditedMoney.setCurrency(creditAmount.getMoney().getCurrency());
			monies.add(creditedMoney);
			balanceAmounts.put(creditType,monies);
		}
		customerBalance.setBalanceAmounts(balanceAmounts);
		return customerBalance;
	}

	private CreditAmount buildCreditAmount(Long creditedAmount, String creditedCurrency, CreditType creditedType, String transactionId) {
		CreditAmount creditAmount = new CreditAmount();
		creditAmount.setCreditType(creditedType);
		Money money = new Money();
		money.setAmount(creditedAmount);
		money.setCurrency(creditedCurrency);
		creditAmount.setMoney(money);
		creditAmount.setTransactionId(transactionId);
		return creditAmount;
	}

	private DebitAmount buildDebitAmount(Long debitedAmount, String debitedCurrency, String invoiceId) {
		DebitAmount debitAmount = new DebitAmount();
		debitAmount.setInvoiceId(invoiceId);
		Money money = new Money();
		money.setAmount(debitedAmount);
		money.setCurrency(debitedCurrency);
		debitAmount.setMoney(money);
		debitAmount.setInvoiceId(invoiceId);
		return debitAmount;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException("Mapping object to string failed with " + e);
		}
	}
}

