# Billing Bank

A demo "bank" application that manages credit and debit transactions. A multhithreaded application designed with
idempotency in mind.

This project is build with spring framework for dependency management and RESTFUL request processing.

### Business Rules

Business rules while executing a given request.

- Credits of different types (``CreditType``) cannot be merged together.
    For e.g. if someone adds a $50 credit of ``CreditType.GIFTCARD``, $50 of ``CreditType.CASH`` and then $10 of ``CreditType.GIFTCARD``,
    then their balance should reflect 2 credits: $60 of ``CreditType.GIFTCARD`` and $50 of ``CreditType.CASH``
- Credits need to be consumed in priority order defined by the ``CreditType`` enum. For e.g. if you have a credit of $10
    ``CreditType.GIFTCARD`` and $20 ``CreditType.CASH``, and a debit request comes in for $15, then you 
    would consume $10 from the ``CreditType.GIFTCARD`` credit and then $5 from the ``CreditType.CASH`` credit.
- Credits need to be consumed in the order they were applied. Which means that if I apply a $10 ``CreditType.GIFTCARD`` 
    credit on 1/5 and a $20 ``CreditType.GIFTCARD`` credit on 1/10, and subsequently a debit request comes in on 1/25 
    for $20, the credit added on 1/5 should be consumed completely and the credit added on 1/10 should be consumed partially.
- If there is not enough credit to be consumed then you would return back an error.
- A credit is considered duplicate if the transactionId has already been applied for the **same creditType** for the same customerAccount. The duplicate call should be idempotent.
- A debit is considered duplicate if the invoiceId has been applied for a given customerAccount. The duplicate call should be idempotent.


### Build, Run and Test

The project uses [gradle](https://gradle.org/) for it's build lifecycle.